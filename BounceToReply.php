<?php
/**
 * BounceToReply : Use existing Bounce email for Reply-To
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2021 Valore Formazione <http://www.valoreformazione.it/>
 * @copyright 2021 Denis Chenu <http://www.sondages.pro>
 * @license GPL
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
class BounceToReply extends PluginBase
{
    protected $storage = 'DbStorage';

    static protected $description = 'Set the Reply-To using Bounce when send email';
    static protected $name = 'BounceToReply';

    /**
     * The settings for this plugin
     */
    protected $settings = array(
        'onBeforeTokenEmail' => array(
            'type' => 'checkbox',
            'label' => 'Set Reply-to when sending an email for token',
            'help' => 'Using the current bounce email',
            'default' => true,
        ),
        'onBeforeSurveyEmail' => array(
            'type' => 'checkbox',
            'label' => 'Set Reply-to when sending an email for survey (admin notification for example)',
            'help' => 'Using the current bounce email',
            'default' => false,
        ),
        'onBeforeEmail' => array(
            'type' => 'checkbox',
            'label' => 'Set Reply-to when sending an email on other situation',
            'help' => 'Using the global bounce email',
            'default' => false,
        ),
    );

    public function init()
    {
        $this->subscribe('beforeTokenEmail');
        $this->subscribe('beforeSurveyEmail');
        $this->subscribe('beforeEmail');
    }

    /**
     * @link https://manual.limesurvey.org/BeforeTokenEmail
     */
    public function beforeTokenEmail()
    {
        if($this->get('onBeforeTokenEmail', null, null, true)) {
             $this->setReplyFromBounce();
        }
    }

    /**
     * @link https://manual.limesurvey.org/beforeSurveyEmail
     */
    public function beforeSurveyEmail()
    {
        if($this->get('onBeforeSurveyEmail', null, null, false)) {
             $this->setReplyFromBounce();
        }
    }

    /**
     * @link https://manual.limesurvey.org/beforeEmail
     */
    public function beforeEmail()
    {
        if($this->get('onBeforeEmail', null, null, false)) {
             $this->setReplyFromBounce();
        }
    }

    /**
    * Set the Reply-To when sending an email for token (invite/remind, register if possible)
    * Construct the email and send it
    * Use event to set send to true (or not)
    */
    public function setReplyFromBounce()
    {        
        $oEvent = $this->getEvent();
        $Bounce = $oEvent->get('bounce');
        if(!LimeMailer::validateAddress($Bounce)) {
            /* If not set or invalid get global one */
            $this->log(sprintf("Invalid bounce email %s for event", $Bounce), 'warning');
            $Bounce = App()->getConfig('siteadminbounce');
            if(!LimeMailer::validateAddress($Bounce)) {
                /* If still invalid get out */
                $this->log(sprintf("Invalid global bounce email %s", $Bounce), 'warning');
                return;
            }
        }
        $From = $oEvent->get('from');
        $fromName='';
        $fromMail = $From;
        if (strpos($fromMail,'<')) {
            $fromName = trim(substr($fromMail,0, strpos($fromMail,'<')-1));
            $fromMail = substr($fromMail,strpos($fromMail,'<')+1,strpos($fromMail,'>')-1-strpos($fromMail,'<'));
        }
        if($fromMail == $Bounce) {
            return;
        }
        $Mailer = $oEvent->get('mailer');
        $Mailer->AddReplyTo($Bounce, $fromName);
    }
}
